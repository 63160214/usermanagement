public class TestUserService {
    public static void main(String[] args){
        UserService.addUser("user2", "password");
        System.out.println(UserService.getUser());
        UserService.addUser(new User("user2", "password"));
        System.out.println(UserService.getUser());

        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("1234");
        UserService.updateUser(3, user);
        System.out.println(UserService.getUser());

        UserService.delUser(user);
        System.out.println(UserService.getUser());

        System.out.println(UserService.login("admin", "password"));
    }
    
}
